#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, const char *argv[]){
    int fd1 = open("fd1", O_RDWR|O_CREAT);
    int fd2 = open("fd2", O_RDWR|O_CREAT);
    dup2(fd1, fd2);
    write(fd2, "hello\n", 7);
    close(fd1);
    return 0;
}