#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <signal.h>

static int count = 0;

static void daemonize(void){
    pid_t pid = -1;
    pid = fork();

    switch( pid ){
        case -1:
            exit(EXIT_FAILURE);
            break;
        case 0:
            break;
        default:
            exit(EXIT_SUCCESS);
            break;
    }

    /*第一子进程成为新的会话组长和进程组长*/
    if( setsid() < 0 )
        exit(EXIT_FAILURE);

    /* Catch, ignore and handle signals */
    //TODO: Implement a working signal handler */
    //signal(SIGCHLD, SIG_IGN);
    //signal(SIGHUP, SIG_IGN);

    /*与控制终端分离*/
    pid = fork();
    switch( pid ){
        case -1:
            exit(EXIT_FAILURE);
            break;
        case 0:
            break;
        default:
            exit(EXIT_SUCCESS);
            break;
    }

    /* Change the working directory to the root directory */
    /* or another appropriated directory */
    chdir("/");

    /*重设文件创建掩模*/
    umask(0);

    /*关闭打开的文件描述符*/
    int i = 0;
    for(i = 0; i < sysconf(_SC_OPEN_MAX); i++){
        close(i);
    }

    /*Attach file descriptors 0,1,and 2 to /dev/null*/
    int fd0 = open("/dev/null", O_RDWR);
    int fd1 = dup(fd0);
    int fd2 = dup(fd0);
    if( fd0 != 0 || fd1 != 1 || fd2 != 2 )
        exit(EXIT_FAILURE);
}

void timer_signal_handler(int signo){
    count ++;
    printf("this is %d time to receive alarm\n", count);
}

static int init_sigaction(){
    struct sigaction sig_action;
    sig_action.sa_handler = timer_signal_handler;
    sig_action.sa_flags = 0;
    sigemptyset(&sig_action.sa_mask);
    sigaction(SIGVTALRM, &sig_action, NULL);

    return 0;
}

void init_signal_timer(){
    struct itimerval timer;
    timer.it_value.tv_sec = 3; //3秒钟后计时器启动
    timer.it_value.tv_usec = 0;

    timer.it_interval.tv_sec = 5; //每5秒钟发送一次SIGALRM信号
    timer.it_interval.tv_usec = 0;

    setitimer(ITIMER_VIRTUAL, &timer, NULL);
}

int main(int argc, const char *argv[]){
    const char *optstring = "vd";
    struct option opts[] = {
        {"version", no_argument, NULL, 'v'},
        {"daemon", no_argument, NULL, 'd'}
    };

    int opt = -1;
    while( (opt = getopt_long(argc, argv, optstring, opts, NULL)) != -1  ){
        switch(opt){
            case 'v':
                printf("version 1\n");
                break;
            case 'd':
                daemonize();
                break;
        }
    }
    
    init_sigaction();
    init_signal_timer();
    while(1);
    return 0;
}
