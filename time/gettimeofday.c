#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

int main(){
#if 0
    time_t tm;
    tm = time(NULL);
    printf("%d\n", tm);
    sleep(2);
    tm = time(NULL);
    printf("%d\n", tm);
#endif
    struct timeval start;
    struct timeval end;
    gettimeofday(&start, NULL);
    sleep(1);
    gettimeofday(&end, NULL);
    printf("%d秒%d毫秒\n", end.tv_sec - start.tv_sec, end.tv_usec - start.tv_usec);

    return 0;
}
