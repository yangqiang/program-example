#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
int count=0;  
int main(){
#if 1
    printf("pid = %d\n", getpid());
    printf("ppid = %d\n", getppid());
    printf("uid = %d\n", getuid());
    printf("euid = %d\n", geteuid());
    printf("gid = %d\n", getgid());
    printf("egid = %d\n", getegid());

    pid_t fpid; //fpid表示fork函数返回的值  

    fpid=fork();   
    if (fpid < 0)   
        printf("error in fork!");   
    else if (fpid == 0) {  
        printf("i am the child process, my process id is %d\n",getpid());   
        printf("我是爹的儿子\n");//对某些人来说中文看着更直白。  
        count++;  
        exit(0);
    }  
    else {  
        printf("i am the parent process, my process id is %d\n",getpid());   
        printf("我是孩子他爹\n");  
        count++;  
        sleep(1);
    }  
    printf("统计结果是: %d\n",count);  
#endif

#if 0
    for(int i=0; i<2; i++){  
        pid_t fpid=fork();//执行完毕，i=0，fpid=0  
        if(fpid==0)  
            printf("%d child  %4d %4d %4d\n",i,getppid(),getpid(),fpid);  
        else  
            printf("%d parent %4d %4d %4d\n",i,getppid(),getpid(),fpid);  
    }
        return 0;
#endif
}
