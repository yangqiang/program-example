# 进程标识

## 进程ID

0：交换进程

1：init进程，以超级用户权限运行，是所有孤儿进程的父进程

2：页守护进程，在某些Unix系统中存在



## 进程函数

```c
//====进程相关函数==============================================================
#include <unistd.h>
//成功返回进程ID，否则返回-1。
pid_t getpid(void); //获取进程ID
pid_t getppid(void);//获取父进程ID

pid_t getuid(void); //获取进程用户ID
pid_t geteuid(void);//获取进程有效用户ID

pid_t getgid(void); //获取进程组ID
pid_t getegid(void);//获取进程有效组ID

pid_t fork(void);   //对父进程，返回新创子进程ID；对子进程，返回0；失败返回-1。
//子进程复制父进程的堆栈段和数据段的内容，但和父进程共用代码段。
// 父子进程的不同点：
// 1. 进程ID和父进程ID不同，但调度机会均等。
// 2. 子进程的time_utime,time_stime,time_cutime,time_ustime被清零。
// 3. fork函数返回值不同。
// 4. 文件锁不会被继承。
// 5. 子进程清理未处理的闹钟信号和未决信号。

// 导致fork失败的原因：
// 1.系统已有太多进程;
// 2.调用fork函数的用户的进程太多;

pid_t vfork(void);
//创建公用父进程地址空间的子进程
// vfork 与 fork 的区别：
// vfork 产生的子进程与父进程公用地址空间，可影响父进程。更像是线程。
// vofrk 产生的子进程一定先运行，父进程等待子进程运行完以后再运行。
// 注意：不要在任何函数中调用vfork;

int setuid(uid_t uid);
//改变进程的实际用户ID和有效用户ID,成功返回0;失败返回-1.

#include <stdlib.h>
void exit(int status);  //退出线程
// 可使用"echo $?"命令来查看程序退出值
// return 1; == exit(1);

#include <sys/wait.h>
pid_t wait(int *statloc);//获取子进程结束状态
// 返回子进程ID，并将结束信息保存在statloc指向的内存空间
// 状态:                  判断宏 :                  取值宏:
// 进程正常退出 : WIFEXITED(status)       WEXITSTATUS(status)
// 进程异常退出 : WIFSIGNALED(status)   WTERMSIG(status)
// 进程暂停       : WIFSTOPPED(status)    WSTOPSIG(status)
// 只能等待第一个退出的进程

pid_t waitpid(pid_t pid, int *statloc, int options);//用于等待指定的进程退出
//参数pid : -1,任意子进程; >0,进程ID == pid; 0, 组ID == pid; < -1, 组ID == pid绝对值;
//参数options : 
//        WCONTINUED  : 当子进程在暂停后继续运行，且其状态未上报，则返回其状态; 
//        WNOHANG     : 当所等待的进程未结束运行时阻塞，waitpid直接返回;
//        WUNTRACED   : 子进程暂停时，其状态一直未上报，则返回其状态;

#include <signal.h>
void (*signal(int signo, void (*func) (int))) (int);
//参数signo : 需要加载处理的信号编号,例如SIGKILL等.编号是整数宏，定义在signal.h中。
//参数func  : 函数指针，捕捉信号后的响应函数,该参数有以下3个可能值 :
    // SIG_IGN : 忽略该信号，该宏在signal.h中定义： #define SIG_IGN ((void *)(*)()) 1
    // SIG_DFL : 默认处理方式，该宏在signal.h中定义： #define SIG_IGN ((void *)(*)()) 0
    // 其他已定义的指针函数 : 信号处理函数原型: void handler(int);
// 函数的返回值是一个函数指针，该函数指向上一次的信号处理程序。该函数与参数func表示的一致
// 如果出错，则signal返回SIG_ERR(在signal.h中定义，#define SIG_ERR ((void *)(*)()) -1)。
//由于signal函数过于复杂，使用typedef进行如下简化:
typedef void HANDLER (int);
HANDLER *signal (int signo, HANDLER *handler);
//Linux不允许用户创建新信号，但提供SIGUSR1和SIGUSR2专门用于应用程序间进行信号通信。

int kill(pid_t pid, int signo);//向另外一个进程发送信号
//参数pid的取值及语义:
// >0,发送给进程ID为pid的进程;
// =0,发送给进程组ID和该进程相同的进程;
// <0,发送给进程组内进程ID为pid的进程;
//=-1,发送给系统所有的进程;
```