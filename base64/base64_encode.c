#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static char base64_chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int base64_encode(const void *srcdata, int size, char *dstdata);
int base64_encode_url(const char *srcdata, int size, char *dstdata);


int main(int argc, char const *argv[]) {
    if(argc != 2 && !argv[1] ){
        printf("error\n");
        exit(1);
    }

    printf("source string: %s\n", argv[1]);

    /*计算经过base64编码后的字符串长度*/
    //int srcsize = sizeof(argv[1]);  //使用sizeof貌似有点问题，可能把\n也计算进去了
    int srcsize = strlen(argv[1]);
    int dstsize =0;
    if( srcsize % 3 == 0 ){
        dstsize = srcsize*4/3;
    }else{
        dstsize = srcsize*4/3 + 4;
    }
    char *base64_encode_str = (char *) malloc(dstsize);
    memset(base64_encode_str, '\0', dstsize);
    base64_encode(argv[1], srcsize, base64_encode_str);
    printf("encode string: %s\n", base64_encode_str);

    free(base64_encode_str);

    return 0;
}

int base64_encode(const void *srcdata, int size, char *dstdata){
    if( !srcdata || !dstdata || size < 0 ){
        return -1;
    }

    const char *s = (const char *)srcdata;
    char *d = dstdata;
    int c;
    for( int i = 0; i < size; i++ ){
        c = s[i];
        c = c << 8;
        i++;
        if( i < size ){
            c += s[i];
        }

        c = c << 8;
        i++;
        if( i < size ){
            c += s[i];
        }

        d[0] = base64_chars[(c & 0x00fc0000) >> 18];
        d[1] = base64_chars[(c & 0x0003f000) >> 12];
        d[2] = base64_chars[(c & 0x00000fc0) >> 6];
        d[3] = base64_chars[(c & 0x0000003f) >> 0];

        d += 4;
    }

    d -= 4;
    switch( size%3 ) {
        case 1:
            d[3] = '=';
        case 2:
            d[2] = '=';
            break;
    }

    s = NULL;

    return 1;

}
