#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static char base64_chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

int pos(char c){
    char *p;
    for (p = base64_chars; *p; p++)
    {
        if (*p == c)
        {
            return p - base64_chars;
        }
    }
    return -1;
}

#define DECODE_ERROR 0xffffffff

unsigned int token_decode(const char *token){
    int i;
    unsigned int val = 0;
    int marker = 0;
    if (!token[0] || !token[1] || !token[2] || !token[3])
    {
        return DECODE_ERROR;
    }
    for (i = 0; i < 4; i++)
    {
        val *= 64;
        if (token[i] == '=')
        {
            marker++;
        }
        else if (marker > 0)
        {
            return DECODE_ERROR;
        }
        else
        {
            val += pos(token[i]);
        }
    }
    if (marker > 2)
    {
        return DECODE_ERROR;
    }
    return (marker << 24) | val;
}
/*
 * Decode base64 str, outputting data to buffer
 * at data of length size.  Return length of
 * decoded data written or -1 on error or overflow.
 */
int base64_decode(const char *str, void *data, int size){
    const char *p;
    unsigned char *q;
    unsigned char *e = NULL;

    q = data;
    if (size >= 0)
    {
        e = q + size;
    }
    for (p = str; *p && (*p == '=' || strchr(base64_chars, *p)); p += 4)
    {
        unsigned int val = token_decode(p);
        unsigned int marker = (val >> 24) & 0xff;
        if (val == DECODE_ERROR)
        {
            return -1;
        }
        if (e && q >= e)
        {
            return -1;
        }
        *q++ = (val >> 16) & 0xff;
        if (marker < 2)
        {
            if (e && q >= e)
            {
                return -1;
            }
            *q++ = (val >> 8) & 0xff;
        }
        if (marker < 1)
        {
            if (e && q >= e)
            {
                return -1;
            }
            *q++ = val & 0xff;
        }
    }
    return q - (unsigned char *) data;
}


int main(int argc, char const *argv[]) {
    if(argc != 2 && !argv[1] ){
        printf("error\n");
        exit(1);
    }

    /*计算经过base64编码后的字符串长度*/
    //int srcsize = sizeof(argv[1]);  //使用sizeof貌似有点问题，可能把\n也计算进去了
    int srclen = strlen(argv[1]);
    if( srclen % 4 != 0 ){
        printf("error \n");
        exit(1);
    }

    char *base64_decode_str = (char *) malloc(srclen);
    memset(base64_decode_str, '\0', srclen);
    int result = base64_decode(argv[1], base64_decode_str, srclen );
    if(result < 0 ){
        printf("error source string\n");
        exit(1);
    }
    printf("decode string: %s\n", base64_decode_str);

    free(base64_decode_str);

    return 0;
}
