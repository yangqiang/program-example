#include <stdio.h>
#include <net/if.h>
#include <string.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ioctl.h>

int main(int argc, const char*argv[]){
    struct ifreq if_req;
    memset(&if_req, 0, sizeof(if_req));
    int fd = -1;

    strcpy(if_req.ifr_name, "ens33");
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if(fd < 0){
        perror("socket error:");
        return 1;
    }

    if(ioctl(fd, SIOCGIFHWADDR,&if_req) < 0)
    {
        perror("error ioctl");
        return 3;
    }
    printf("%02x:%02x:%02x:%02x:%02x:%02x\n",
           (unsigned char)if_req.ifr_hwaddr.sa_data[0],
           (unsigned char)if_req.ifr_hwaddr.sa_data[1],
           (unsigned char)if_req.ifr_hwaddr.sa_data[2],
           (unsigned char)if_req.ifr_hwaddr.sa_data[3],
           (unsigned char)if_req.ifr_hwaddr.sa_data[4],
           (unsigned char)if_req.ifr_hwaddr.sa_data[5]);
    return 0;
}
